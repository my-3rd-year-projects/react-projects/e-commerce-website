extensions used

1. ES7 + React/Redux/React-Native Snippets
2. Auto Import - ES6, TS, JSX, TSX
3. Auto Complete tag
4. DotENV
5. html to JSX
6. Prettier

Settings:
format on save

packages used:
1. express
2. colors
3. nodemon
4. dotenv
5. mongoose
6. morgan
7. bcrypt
8. jsonwebtoken