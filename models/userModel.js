import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required'],
        trim: true //whitespaces are removed
    },
    email: {
        type: String,
        required: [true, 'email is required'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    mobile: {
        type: String,
        required: [true, 'Mobile Number is required']
    },
    address: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    }

}, { timestamps: true });

export default mongoose.model('users', userSchema);