// const express = require('express');
// const colors = require('colors');

// It is only for commonjs module

import express from 'express';
import colors from 'colors';
import dotenv from 'dotenv';
import morgan from 'morgan';
import { connectDB } from './config/db.js';
import authRoutes from './routes/authRoute.js';

// configure env
dotenv.config();

// database config
connectDB();

// rest object
const app = express();

// middleware
app.use(express.json()); // we used to use bodyparser but now by default, we get this function with express
app.use(morgan('dev'));

// routes
app.use('/auth', authRoutes);

// rest api
app.get('/', (req, res) => {
    res.send(
        // {message: 'Welcome to my E-Commerce MERN Stack app'}
        '<h1>Welcome to my E-Commerce MERN Stack App</h1>'
    )
})

// PORT
const PORT = process.env.PORT || 8080

// run-listen
app.listen(PORT, () => {
    console.log(`Server running on PORT: ${PORT}`.bgCyan.white);
})