import { comparePassword, hashPassword } from "../helpers/authHelper.js";
import userModel from "../models/userModel.js";
import jwt from 'jsonwebtoken';

export const registerController = async (req, res) => {
    try {
        const { name, email, password, mobile, address } = req.body;
        
        // validation
        if (!name) {
            return res.send({ error: "Name is required" });
        }

        if (!email) {
            return res.send({ error: "Email is required" });
        }

        if (!password) {
            return res.send({ error: "Password is required" });
        }

        if (!mobile) {
            return res.send({ error: "Mobile number is required" });
        }

        if (!address) {
            return res.send({ error: "Address is required" });
        }

        // check existing user
        const existingUser = await userModel.findOne({email});
        // existing user
        if (existingUser) {
            res.status(200).send({
                success: false,
                message: 'You have already registered. Kindly Login',
            })
        } else {
            const hashedPassword = await hashPassword(password);
            // save
            const user = await new userModel({ name, email, password: hashedPassword, mobile, address }).save();

            res.status(201).send({
                success: true, 
                message: 'Congratulations! You registered successfully',
                user
            })
        }

    } catch (error) {
        console.log(error);
        res.status(500).send({
            success: false,
            message: 'Error in Registration',
            error
        })
    }
}

// LOGIN || POST
export const loginController = async (req, res) => {
    try {
        const { email, password } = req.body;
        // validation
        if (!email || !password) {
            return res.status(404).send({
                success: false,
                message: 'Invalid Email or Password'
            })
        }
        // check user
        const user = await userModel.findOne({ email });
        if (!user) {
            res.status(404).send({
                success: false,
                message: 'You have not registered. Please register'
            })
        } else {
            const match = await comparePassword(password, user.password);
            if (!match) {
                return res.status(404).send({
                    success: false,
                    message: 'User Credentials wrong. Please try again later'
                })
            } else {
                return res.status(200).send({
                    success: true,
                    message: 'Congratulations! You have successfully logged in',
                    user
                })
            }
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({
            success: false,
            message: "Error in login",
            error
        })
    }
} 